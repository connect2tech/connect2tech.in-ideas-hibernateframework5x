package com.c2t.hbm.onemany.bi;

import org.hibernate.Session;
import org.testng.annotations.Test;

public class OneMany {

	@Test
	public void fetchStockDetails_Lazy() {
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = session.get(Stock.class, 1);

		session.getTransaction().commit();
		System.out.println("Done");
	}
}
