package com.c2t.hbm.oneone.bi;

import java.util.Date;

import org.hibernate.Session;

public class Create2 {
	public static void main(String[] args) {
		System.out.println("Hibernate one to one (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();

		stock.setStockCode("7055");
		stock.setStockName("PADINI-5");
		session.save(stock);
		
		session.getTransaction().commit();

		System.out.println("Done");
	}
}
