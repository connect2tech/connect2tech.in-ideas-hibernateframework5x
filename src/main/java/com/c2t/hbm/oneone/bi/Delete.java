package com.c2t.hbm.oneone.bi;

import java.util.Date;

import org.hibernate.Session;

public class Delete {
	public static void main(String[] args) {
		System.out.println("Hibernate one to one (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock s = session.get(Stock.class, 1);

		if (s != null) {
			session.delete(s);
		}

		session.getTransaction().commit();

		System.out.println("Done");
	}
}
