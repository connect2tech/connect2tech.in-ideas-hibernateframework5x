package com.c2t.hbm.oneone.bi;

import java.util.Date;

import org.hibernate.Session;

public class Create {
	public static void main(String[] args) {
		System.out.println("Hibernate one to one (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();

		stock.setStockCode("id-"+ new Date().toString());
		stock.setStockName("name-"+ new Date().toString());

		StockDetail stockDetail = new StockDetail();
		stockDetail.setCompName("PADINI Holding Malaysia1");
		stockDetail.setCompDesc("one stop shopping1");
		stockDetail.setRemark("vinci vinci1");
		stockDetail.setListedDate(new Date());

		stock.setStockDetail(stockDetail);
		stockDetail.setStock(stock);
		session.save(stock);
		
		session.getTransaction().commit();

		System.out.println("Done");
	}
}
