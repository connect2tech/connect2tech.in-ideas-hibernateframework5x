package com.c2t.hbm.onemany.bi;

import java.util.Date;

import org.hibernate.Session;

public class Create {
	public static void main(String[] args) {
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7052");
        stock.setStockName("PADINI");
        session.save(stock);
        
        StockDailyRecord stockDailyRecords = new StockDailyRecord();
        stockDailyRecords.setPriceOpen(new Float("1.2"));
        stockDailyRecords.setPriceClose(new Float("1.1"));
        stockDailyRecords.setPriceChange(new Float("10.0"));
        stockDailyRecords.setVolume(3000000L);
        stockDailyRecords.setDate(new Date());
        
        stockDailyRecords.setStock(stock);       
        
        StockDailyRecord stockDailyRecords2 = new StockDailyRecord();
        stockDailyRecords2.setPriceOpen(new Float("1.2"));
        stockDailyRecords2.setPriceClose(new Float("1.1"));
        stockDailyRecords2.setPriceChange(new Float("10.0"));
        stockDailyRecords2.setVolume(3000000L);
        
        Date today=new Date();
        long ltime=today.getTime()+24*60*60*1000;
        Date today_1=new Date(ltime);
        stockDailyRecords2.setDate(today_1);
        
        stockDailyRecords2.setStock(stock);       
        
        stock.getStockDailyRecords().add(stockDailyRecords);
        stock.getStockDailyRecords().add(stockDailyRecords2);

        session.save(stock);

		session.getTransaction().commit();
		System.out.println("Done");
	}
}
