package com.c2t.hbm.onemany.bi;

import java.util.Date;

import org.hibernate.Session;

public class Delete {
	public static void main(String[] args) {
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock =  session.get(Stock.class, 1);
		session.delete(stock);

		session.getTransaction().commit();
		System.out.println("Done");
	}
}
