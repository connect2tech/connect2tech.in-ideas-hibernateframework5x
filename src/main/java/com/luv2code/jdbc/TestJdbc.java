package com.luv2code.jdbc;

import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {

		String jdbcUrl = "jdbc:mysql://localhost:3306/ideas";
		String user = "root";
		String pass = "password";

		try {
			System.out.println("Connecting to database: " + jdbcUrl);

			DriverManager.getConnection(jdbcUrl, user, pass);

			System.out.println("Connection successful!!!");

		} catch (Exception exc) {
			exc.printStackTrace();
		}

	}

}
