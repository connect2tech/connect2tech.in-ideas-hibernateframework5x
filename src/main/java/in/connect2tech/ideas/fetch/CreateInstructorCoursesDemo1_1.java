package in.connect2tech.ideas.fetch;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateInstructorCoursesDemo1_1 {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("in/connect2tech/ideas/fetch/hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(Course.class).buildSessionFactory();

		// create session
		Session session = factory.openSession();

		try {

			// start a transaction
			session.beginTransaction();

			// create instructor
			Instructor tempInstructor = new Instructor("Daffy", "Duck", "daffy.duck@luv2code.com");

			session.save(tempInstructor);

			// create some courses
			Course tempCourse1 = new Course("Duck training - volume 1");
			Course tempCourse2 = new Course("Duck training - volume 2");

			// add courses to instructor
			tempInstructor.add(tempCourse1);
			tempInstructor.add(tempCourse2);

			// save the courses
			session.save(tempCourse1);
			session.save(tempCourse2);

			// commit transaction
			session.getTransaction().commit();

			System.out.println("Done!");
		} finally {

			// add clean up code
			// session.close();

			// factory.close();
		}
	}

}
