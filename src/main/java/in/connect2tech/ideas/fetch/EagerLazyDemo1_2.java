package in.connect2tech.ideas.fetch;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EagerLazyDemo1_2 {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("in/connect2tech/ideas/fetch/hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(Course.class).buildSessionFactory();

		// create session
		Session session = factory.openSession();

		try {

			// start a transaction
			session.beginTransaction();

			// get the instructor from db
			int theId = 1;
			Instructor tempInstructor = session.get(Instructor.class, theId);
			
			System.out.println("-----------------11111111111111----------------------");

			System.out.println("luv2code: Instructor: " + tempInstructor);
			
			// commit transaction
			//session.getTransaction().commit();



			
			System.out.println("-----------------222222222222222----------------------");
			
			
			System.out.println("luv2code: Courses: " + tempInstructor.getCourses());
			
			// close the session
			session.close();
			
			
			/*System.out.println("-----------------3333333333333----------------------");
			// get courses for the instructor
			System.out.println("luv2code: Courses: " + tempInstructor.getCourses());

			

			System.out.println("\nluv2code: The session is now closed!\n");*/

			// option 1: call getter method while session is open
			
			

			System.out.println("luv2code: Done!");
		} finally {

			// add clean up code
			// session.close();

			// factory.close();
		}
	}

}
