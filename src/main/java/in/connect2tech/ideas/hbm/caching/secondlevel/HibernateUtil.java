package in.connect2tech.ideas.hbm.caching.secondlevel;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			// return new AnnotationConfiguration().configure(new
			// File("hibernate.cgf.xml")).buildSessionFactory();

			// create session factory
			SessionFactory factory = new Configuration()
					.configure("in/connect2tech/ideas/hbm/caching/secondlevel/hibernate-cache.cfg.xml")
					.addAnnotatedClass(DepartmentEntity.class)
					.buildSessionFactory();
			
			
			return factory;

		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
